

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1500 );
var mesh;

var renderer = new THREE.WebGLRenderer({alpha: true});
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// add icosahedron
const geometry = new THREE.IcosahedronGeometry(20);
THREE.ImageUtils.crossOrigin = true;
var textureLoader = new THREE.TextureLoader();
textureLoader.crossOrigin = true;
textureLoader.load('https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/442815/1160/772/m1/fpnw/wm0/3_p-.jpg?1428833025&s=f787100c6ea867798403748bd025f286', function(texture) {
  texture.wrapS = texture.wrapT =   THREE.RepeatWrapping;
    texture.repeat.set( 2, 2 );
    var material = new THREE.MeshLambertMaterial( {map: texture} );
  mesh = new THREE.Mesh( geometry, material );
  scene.add( mesh );
  
  render();
});


camera.position.z = 120;

// so many lights
var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 0, 1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, -1, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 1, 0, 0 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( 0, 0, 1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 1 );
light.position.set( 0, 0, -1 );
scene.add( light );

var light = new THREE.DirectionalLight( 0xffffff, 0.5 );
light.position.set( -1, 0, 0 );
scene.add( light );


var render = function () {
  requestAnimationFrame( render );
  //mesh.rotation.x += 0.05;
  mesh.rotation.x += 0.03;
  mesh.rotation.y -= 0.03;
  renderer.render(scene, camera);
};





// const w = window.innerWidth;
// const h = window.innerHeight;

// const scene = new THREE.Scene();
// const renderer = new THREE.WebGLRenderer();
// const camera = new THREE.PerspectiveCamera(30, w/h, 0.1, 1000);
// const light = new THREE.PointLight(0xFFFF00);

// const material = new THREE.MeshNormalMaterial();

// const geometry1 = new THREE.IcosahedronGeometry(20, 0);
// const shape1 = new THREE.Mesh(geometry1, material);

// const geometry2 = new THREE.TorusGeometry(32, 3, 16, 100);
// const shape2 = new THREE.Mesh(geometry2, material);

// for (var i = 0, l = geometry1.vertices.length; i<l; i++) {
//   // we'll move the x & y position of each vertice by a random amount
//   geometry1.vertices[i].x += -10 + Math.random()*20;
//   geometry1.vertices[i].y += -10 + Math.random()*20;
// }

// renderer.setSize(w, h);
// document.body.appendChild(renderer.domElement);

// shape1.rotation.x = 0.1;
// shape1.rotation.y = -0.25;

// camera.position.z = 200;

// light.position.set( 10, 0, 25 );

// scene.add(shape1);
// scene.add(shape2);
// scene.add(light);

// renderer.render(scene, camera);

// const animate = function () {
//   requestAnimationFrame(animate);

//   shape1.rotation.x += 0.05;
//   shape2.rotation.y -= 0.05;
//   shape2.rotation.x -= 0.05;
  
//   renderer.render(scene, camera);
// };

// animate();